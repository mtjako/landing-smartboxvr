<!DOCTYPE html>
<html lang="pl">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Okulary VR</title>
    <link rel="stylesheet" href="css/style.css">
</head>
<body>
    <nav>
        <a href="https://smartboxvr.com/" class="logo"><img src="/ankieta/img/logo.png" alt="Logo"></a>
        <div class="menubtn"></div>
    </nav>
    <ul class="menu">
        <li class="menu__item"><a target="_blank" href="https://smartboxvr.com/pl/" class="menu__item__link">Home</a></li>
        <li class="menu__item"><a target="_blank" href="https://smartboxvr.com/pl/?smartbox" class="menu__item__link">Smartbox</a></li>
        <li class="menu__item"><a target="_blank" href="https://smartboxvr.com/pl/?realizacje" class="menu__item__link">Realizacje</a></li>
        <li class="menu__item"><a target="_blank" href="https://smartboxvr.com/pl/jak-zamowic" class="menu__item__link">Jak zamówić?</a></li>
        <li class="menu__item"><a target="_blank" href="https://smartboxvr.com/pl/o-nas" class="menu__item__link">O nas</a></li>
        <li class="menu__item"><a target="_blank" href="https://smartboxvr.com/pl/faq" class="menu__item__link">FAQ</a></li>
        <li class="menu__item"><a target="_blank" href="https://smartboxvr.com/pl/faq" class="menu__item__link">Kontakt</a></li>
    </ul>
    <div class="img">
    <img src="/ankieta/img/main-photo.png" alt="Okulary VR">
    <div class="box">
        <p>Zestaw sampli + prezent niespodzianka</p>
        <div class="box__wrap">
            <div>
                <img class="box__vr" src="/ankieta/img/1.png" alt="box">
                <img class="box__vr" src="/ankieta/img/3.png" alt="box">
            </div>
            <div class="box__plus">+</div>
            <div>
                <img class="box__suprise" src="/ankieta/img/box.png" alt="box">
            </div>
        </div>
    </div>
</div>
    <main class="main">

        <!-- <div class="main__description">
            <p>Zostaw swoje dane i bądźmy w kontakcie!</p>
        </div> -->
        <?php if(!empty($_POST)): ?>
        <h2>Dziękujemy</h2>
        <?php
            require  __DIR__ . '/vendor/autoload.php';
            $date = new DateTime();
            $mail = new \PHPMailer\PHPMailer\PHPMailer(true);
            $mail->isSMTP();
            $mail->SMTPOptions = array(
            'ssl' => array(
            'verify_peer' => false,
            'verify_peer_name' => false,
            'allow_self_signed' => true
            )
            );
            $mail->SMTPAutoTLS = false;
            $mail->CharSet = 'UTF-8';
            $mail->Host       = 'serwer1683753.home.pl';
            $mail->SMTPDebug  = 0;
            $mail->AddAddress('l.webmasteer@gmail.com');
            $mail->SMTPAuth   = true;
            $mail->SMTPSecure = "tls";
            $mail->Host       = 'serwer1683753.home.pl';
            $mail->Port       = 587;
            $mail->Username   = 'contact@smartboxvr.com';
            $mail->Password   =  'FDP29rZZ,l,_';
            $mail->Subject    = "Ankieta";
            $mail->FromName   = mb_convert_encoding("Ankieta", "UTF-8", "auto");
            $mail->From       = 'info@einity.pl';
            $mail->SetLanguage("pl", "PHPMailer/language/");
            $mail->AddReplyTo( 'program@fast.zgora.pl', 'Liga Fachowców' );
            $mail->MsgHTML('
            <strong>Ankieta</strong>
            <p>Imię: '.$_POST["name"].'</p>
            <p>Nazwisko: '.$_POST["surname"].'</p>
            <p>Nazwa firmy: '.$_POST["company-name"].'</p>
            <p>Ulica: '.$_POST["street"].'</p>
            <p>Miasto: '.$_POST["city"].'</p>
            <p>Kod pocztowy: '.$_POST["post-code"].'</p>
            <p>Telefon: '.$_POST["phone"].'</p>
            <p>Email: '.$_POST["email"].'</p>
            ');
            $mail -> Send();
            ?>
        <?php else: ?>
        <div class="main__header">
            <h1>Jeżeli chcesz otrzymać zestaw sampli różnych modeli okularów VR oraz prezent niespodziankę zostaw swoje dane!</h1>
        </div>
        <form action="https://smartboxvr.com/ankieta/" method="post">
        <div>
            <input required type="text" name="name" id="name" placeholder="Imię">
            <input required type="text" name="surname" id="surname" placeholder="Nazwisko">
        </div>
        <div><input required type="text" name="company-name" id="company-name" placeholder="Nazwa firmy"></div>

        <div><input required type="text" name="street" id="street" placeholder="Ulica"></div>
        <div>
            <input required type="text" name="city" id="city" placeholder="Miasto">
            <input required type="text" name="post-code" id="post-code" placeholder="Kod pocztowy">
        </div>
        <div><input required type="text" name="phone" id="phone" placeholder="Numer Telefonu"></div>
        <div><input required type="email" name="email" id="email" placeholder="Email"></div>
        <div>
            <input required type="checkbox" name="consent" id="consent">
            <label for="consent">* Zgadzam się na otrzymywanie cyklicznych wiadomości o promocjach,
                nowych usługach, materiałów promocyjnych, itp. na podany przeze mnie adres
                e-mail przez EINITY sp z o.o., zgodnie z Regulaminem. Wiem, że zgoda jest
                dobrowolna i mogę ją wycofać w dowolnym momencie klikając w link zawarty w
                przekazywanych wiadomościach.</label>
        </div>
        <div>
            <input type="submit" value="Wyślij">
        </div>
    </form>
        <?php endif; ?>
    </main>
</body>
<script src="/script/main.js"></script>
</html>