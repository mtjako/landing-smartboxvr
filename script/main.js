const btn = document.querySelector(".menubtn");
const menu = document.querySelector(".menu");
const menuitem = document.querySelectorAll(".menu__item");

btn.addEventListener("click", () => {
  menu.classList.toggle("open");
});

menuitem.forEach((element) => {
  element.addEventListener("click", () => {
    menu.classList.toggle("open");
  });
});
